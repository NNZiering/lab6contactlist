package th.ac.tu.siit.lab6contactlist;
import java.io.FileNotFoundException;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.*;
import android.widget.*;

public class AddNewActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new);
		EditText etName = (EditText)findViewById(R.id.etName);
		EditText etPhone = (EditText)findViewById(R.id.etPhone);
		//RadioGroup rdgType = (RadioGroup)findViewById(R.id.rdgType);
		RadioButton rdHome = (RadioButton)findViewById(R.id.rdHome);
		RadioButton rdMobile = (RadioButton)findViewById(R.id.rdMobile);
		
		EditText etEmail = (EditText)findViewById(R.id.etEmail);
		
		Intent d = this.getIntent();
		if(d.hasExtra("pos")){//PROBABLY EDIT
			String f1 = d.getStringExtra("name");
			etName.setText(f1);
			String f2 = d.getStringExtra("phone");
			etPhone.setText(f2);
			int f3 = Integer.parseInt(d.getStringExtra("type"));
			//etPhone.setText(f3+","+R.drawable.home);
			if (f3==R.drawable.home) {
				rdHome.setChecked(true);
			}
			else if (f3==R.drawable.mobile) {
				rdMobile.setChecked(true);
			}
			try{
				String f4 = d.getStringExtra("email");	
				etEmail.setText(f4);
			}	catch (NullPointerException e) {}
			
		
			
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_new, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_save) {
			returnRecord();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void returnRecord() {
		EditText etName = (EditText)findViewById(R.id.etName);
		EditText etPhone = (EditText)findViewById(R.id.etPhone);
		RadioGroup rdgType = (RadioGroup)findViewById(R.id.rdgType);
		
		String sName = etName.getText().toString().trim();
		String sPhone = etPhone.getText().toString().trim();
		int iType = rdgType.getCheckedRadioButtonId();
	
		
		if (sName.length() == 0 || sPhone.length() == 0 || iType == -1) {
			AlertDialog dialog = new AlertDialog.Builder(this).create();
			dialog.setTitle("Error");
			dialog.setMessage("All fields are required.");
			dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK", 
				new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) 
				{}});
			dialog.show();
		}
		else {
			Intent data = new Intent();
			data.putExtra("name", sName);
			data.putExtra("phone", sPhone);	
			
			Intent d = this.getIntent();
			int sPos = d.getIntExtra("pos", 0);
			data.putExtra("pos", sPos);	
			
			try{
				EditText etEmail = (EditText)findViewById(R.id.etEmail);
				String sEmail = etEmail.getText().toString().trim();
				data.putExtra("email", sEmail);
			} catch(NullPointerException e){}
			
			String sType = "";
			switch(iType) {
			case R.id.rdHome:
				sType = "home";
				break;
			case R.id.rdMobile:
				sType = "mobile";
				break;
			}
			data.putExtra("type", sType);
			this.setResult(RESULT_OK, data);
			this.finish();
			Toast t = Toast.makeText(this, "A new contact added.", 
					Toast.LENGTH_LONG);
			t.show();
		}
	}
}
